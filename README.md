CONTENTS OF THIS FILE
---------------------

* Introduction
* Recommended modules
* Requirements
* Installation
* Configuration
* Troubleshooting
* Maintainers
* Thanks

INTRODUCTION
------------

Provides a fuzzy search on entity label. By using field widget provided by the
module on the entity reference field, we can support fuzzy search on entity
reference autocomplete results.

* For a full description of the module, visit the project page:
  https://www.drupal.org/project/fuzzy_entity_reference

* To submit bug reports and feature suggestions, or to track changes:
  https://www.drupal.org/project/issues/fuzzy_entity_reference

RECOMMENDED MODULES
-------------------

* No extra module is required.

REQUIREMENTS
------------
* No additional modules are required apart from field and entity_reference.
* Apart from these modules, we need https://github.com/loilo/Fuse library, which
  will be downloaded by module's composer.json

INSTALLATION
------------

* Install as usual, see
  https://www.drupal.org/docs/extending-drupal/installing-modules for further
  information.

CONFIGURATION
-------------

* This module provides a `Field Widget` plugin for the entity reference fields.
* You can visit form display page for any content type or entity.
* Select the `Fuzzy Autocomplete` field widget on entity reference field.
* Select other options depending on the requirement.
  * `Query count limit` option
    * This option allows us to provide list of node title to load on initially
    for passing it to Fuse class instance.
    * Fuse class does the approximate search of all the loaded titles and gives
    the most relevant match.
    * For more close matches, the higher the number is better.

TROUBLESHOOTING
---------------

*


MAINTAINERS
-----------

Current maintainers:

* Mohit Aghera (mohit_aghera) (https://www.drupal.org/user/639098)

THANKS
-----------
Special thanks to following folks for idea and guidance:

* Rikki (rikki_iki) (https://www.drupal.org/user/1068918/)
* Lee Rowlands (larowlan) (https://www.drupal.org/user/395439)
