<?php

declare(strict_types=1);

namespace Drupal\fuzzy_entity_reference\FunctionalJavascript;

use Drupal\fuzzy_entity_reference\Plugin\EntityReferenceSelection\FuzzyEntityAutocomplete;
use Drupal\Tests\field\Traits\EntityReferenceTestTrait;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\Tests\node\Traits\ContentTypeCreationTrait;
use Drupal\Tests\node\Traits\NodeCreationTrait;

/**
 * Tests the output of entity reference autocomplete widgets.
 *
 * @group fuzzy_entity_reference
 */
class FuzzyEntityReferenceAutocompleteWidgetTest extends WebDriverTestBase {

  use ContentTypeCreationTrait;
  use EntityReferenceTestTrait;
  use NodeCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['node', 'field_ui', 'fuzzy_entity_reference'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Array of all the page node titles.
   *
   * @var string[]
   */
  public array $titles;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create a Content type and two test nodes.
    $this->createContentType(['type' => 'page']);

    // Create multiple notes with titles.
    $this->titles = [
      'She had a little puppy',
      'Waste To Energy Facility Planned For Wyndham',
      'The little boat sailed away.',
      'All Abilities Regional Inclusion Week',
      'Inquisitive minds crave literary exploration',
      'The bibliophile\'s home exudes literacy.',
      'His eloquent speech showcased literacy.',
      'The book club fosters literacy.',
      'Vocabulary enhances your literacy skills',
      'Writing promotes your literacy growth.',
      'Conserving energy is a sustainability practice',
      'Equality is for everyone',
      'Online Financial Literacy Seminar',
      'Lit',
      'Little River Community and Nature Expo',
      'The little town was charming',
    ];
    foreach ($this->titles as $title) {
      $this->createNode(['title' => $title]);
    }

    $user = $this->drupalCreateUser([
      'access content',
      'create page content',
    ]);
    $this->drupalLogin($user);
  }

  /**
   * Tests that the default autocomplete widget return the correct results.
   */
  public function testEntityReferenceAutocompleteWidget() {
    $autocomplete_results = [];
    /** @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface $display_repository */
    $display_repository = \Drupal::service('entity_display.repository');

    // Create an entity reference field and use the default 'CONTAINS' match
    // operator.
    $field_name = 'field_test';
    $this->createEntityReferenceField('node', 'page', $field_name, $field_name, 'node', FuzzyEntityAutocomplete::PLUGIN_ID, ['target_bundles' => ['page' => 'page']]);
    $form_display = $display_repository->getFormDisplay('node', 'page');
    $form_display->setComponent($field_name, [
      'type' => 'fuzzy_autocomplete',
      'settings' => [
        'match_operator' => 'CONTAINS',
        'query_count_limit' => 200,
        'match_limit' => 5,
      ],
    ]);
    // To satisfy config schema, the size setting must be an integer, not just
    // a numeric value. See https://www.drupal.org/node/2885441.
    $this->assertIsInt($form_display->getComponent($field_name)['settings']['size']);
    $form_display->save();
    $this->assertIsInt($form_display->getComponent($field_name)['settings']['size']);

    // Visit the node add page.
    $this->drupalGet('node/add/page');
    $page = $this->getSession()->getPage();
    $assert_session = $this->assertSession();

    $autocomplete_field = $assert_session->waitForElement('css', '[name="' . $field_name . '[0][target_id]"].ui-autocomplete-input');
    $autocomplete_field->setValue('Lit');
    $this->getSession()->getDriver()->keyDown($autocomplete_field->getXpath(), ' ');
    $assert_session->waitOnAutocomplete();

    $results = $page->findAll('css', '.ui-autocomplete li');
    foreach ($results as $result) {
      $autocomplete_results[] = $result->getText();
    }

    // Expected results when input keyword is "Lit".
    // Fuse js php library will prioritise the results accordingly.
    $expected = [
      'Lit',
      'Little River Community and Nature Expo',
      'Equality is for everyone',
      'The little boat sailed away.',
      'The little town was charming',
    ];
    $this->assertEquals($expected, $autocomplete_results);

    // Now do another test with keyword "Lite".
    $this->drupalGet('node/add/page');
    $page = $this->getSession()->getPage();
    $assert_session = $this->assertSession();
    $autocomplete_field = $assert_session->waitForElement('css', '[name="' . $field_name . '[0][target_id]"].ui-autocomplete-input');
    $autocomplete_field->setValue('Lite');
    $this->getSession()->getDriver()->keyDown($autocomplete_field->getXpath(), ' ');
    $assert_session->waitOnAutocomplete();

    $autocomplete_results = [];
    $results = $page->findAll('css', '.ui-autocomplete li');
    foreach ($results as $result) {
      $autocomplete_results[] = $result->getText();
    }

    // Expected results when input keyword is "Lite".
    // Fuse js php library will prioritise the results accordingly.
    $expected = [
      'Online Financial Literacy Seminar',
      'The book club fosters literacy.',
      'Writing promotes your literacy growth.',
      'Inquisitive minds crave literary exploration',
      'Vocabulary enhances your literacy skills',
    ];
    // Note that string "The bibliophile\'s home exudes literacy." is not
    // appearing in list despite match because 'literacy' word is at last. So
    // distance is farthest from the matching string.
    $this->assertEquals($expected, $autocomplete_results);

    // Now do another test with keyword "Litt".
    $this->drupalGet('node/add/page');
    $page = $this->getSession()->getPage();
    $assert_session = $this->assertSession();
    $autocomplete_field = $assert_session->waitForElement('css', '[name="' . $field_name . '[0][target_id]"].ui-autocomplete-input');
    $autocomplete_field->setValue('Litt');
    $this->getSession()->getDriver()->keyDown($autocomplete_field->getXpath(), ' ');
    $assert_session->waitOnAutocomplete();

    $autocomplete_results = [];
    $results = $page->findAll('css', '.ui-autocomplete li');
    foreach ($results as $result) {
      $autocomplete_results[] = $result->getText();
    }

    // Expected results when input keyword is "Litt".
    // Fuse js php library will prioritise the results accordingly.
    $expected = [
      'Little River Community and Nature Expo',
      'The little boat sailed away.',
      'The little town was charming',
      'She had a little puppy',
    ];
    // Note that string "The bibliophile\'s home exudes literacy." is not
    // appearing in list despite match because 'literacy' word is at last. So
    // distance is farthest from the matching string.
    $this->assertEquals($expected, $autocomplete_results);
  }

  /**
   * Tests the cache feature of the fuzzy_entity_reference autocomplete widget.
   */
  public function testEntityReferenceAutocompleteCache(): void {
    $autocomplete_results = [];
    /** @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface $display_repository */
    $display_repository = \Drupal::service('entity_display.repository');

    // Create an entity reference field and use the default 'CONTAINS' match
    // operator.
    $field_name = 'field_test';
    $this->createEntityReferenceField('node', 'page', $field_name, $field_name, 'node', FuzzyEntityAutocomplete::PLUGIN_ID, ['target_bundles' => ['page' => 'page']]);
    $form_display = $display_repository->getFormDisplay('node', 'page');
    $form_display->setComponent($field_name, [
      'type' => 'fuzzy_autocomplete',
      'settings' => [
        'match_operator' => 'CONTAINS',
        'query_count_limit' => 200,
        'match_limit' => 20,
        'size' => 60,
      ],
    ]);
    // To satisfy config schema, the size setting must be an integer, not just
    // a numeric value. See https://www.drupal.org/node/2885441.
    $this->assertIsInt($form_display->getComponent($field_name)['settings']['size']);
    $form_display->save();
    $this->assertIsInt($form_display->getComponent($field_name)['settings']['size']);

    // Visit the node add page.
    $this->drupalGet('node/add/page');
    $page = $this->getSession()->getPage();
    $assert_session = $this->assertSession();

    $autocomplete_field = $assert_session->waitForElement('css', '[name="' . $field_name . '[0][target_id]"].ui-autocomplete-input');
    $autocomplete_field->setValue('Lit');
    $this->getSession()->getDriver()->keyDown($autocomplete_field->getXpath(), ' ');
    $assert_session->waitOnAutocomplete();

    $cid = "fuzzy_entity_reference:node:Lit";
    $cache = \Drupal::cache()->get($cid);
    $this->assertNotEmpty($cache->data);
  }

  /**
   * Executes an autocomplete on a given field and waits for it to finish.
   *
   * @param string $field_name
   *   The field name.
   */
  protected function doAutocomplete($field_name) {
    $autocomplete_field = $this->getSession()->getPage()->findField($field_name . '[0][target_id]');
    $autocomplete_field->setValue('Test');
    $this->getSession()->getDriver()->keyDown($autocomplete_field->getXpath(), ' ');
    $this->assertSession()->waitOnAutocomplete();
  }

}
