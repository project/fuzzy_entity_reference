<?php

declare(strict_types=1);

namespace Drupal\fuzzy_entity_reference;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Tags;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityAutocompleteMatcherInterface;
use Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface;
use Drupal\fuzzy_entity_reference\Plugin\EntityReferenceSelection\FuzzyEntityAutocomplete;
use Fuse\Fuse;

/**
 * Service class to decorate entity_autocomplete.matcher service.
 */
class FuzzyAutocompleteMatcherDecorator implements EntityAutocompleteMatcherInterface {

  /**
   * The autocomplete matcher for entity references.
   *
   * @var \Drupal\Core\Entity\EntityAutocompleteMatcherInterface
   */
  protected EntityAutocompleteMatcherInterface $matcher;

  /**
   * The entity reference selection handler plugin manager.
   *
   * @var \Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface
   */
  protected SelectionPluginManagerInterface $selectionManager;

  /**
   * The cache backend service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected CacheBackendInterface $cache;

  /**
   * Constructs a EntityIdAutocompleteMatcher object.
   *
   * @param \Drupal\Core\Entity\EntityAutocompleteMatcherInterface $matcher
   *   The autocomplete matcher for entity references.
   * @param \Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface $selection_manager
   *   The entity reference selection handler plugin manager.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache backend service.
   */
  public function __construct(EntityAutocompleteMatcherInterface $matcher, SelectionPluginManagerInterface $selection_manager, CacheBackendInterface $cache) {
    $this->matcher = $matcher;
    $this->selectionManager = $selection_manager;
    $this->cache = $cache;
  }

  /**
   * {@inheritdoc}
   */
  public function getMatches($target_type, $selection_handler, $selection_settings, $string = ''): array {
    if (empty($string)) {
      return [];
    }

    if ($selection_handler !== FuzzyEntityAutocomplete::PLUGIN_ID) {
      return $this->matcher->getMatches($target_type, $selection_handler, $selection_settings, $string);
    }
    // Get default result.
    $options = $selection_settings + [
      'target_type' => $target_type,
      'handler' => $selection_handler,
    ];
    $handler = $this->selectionManager->getInstance($options);
    assert($handler instanceof FuzzyEntityAutocomplete);

    $matches = $this->fetchEntityLabels($string, $target_type, $selection_settings, $handler);
    if (empty($matches)) {
      return [];
    }

    // Initialize Fuse object and search string.
    $options = [
      'keys' => ['label'],
    ];
    $fuse = new Fuse($matches, $options);
    $matches = $fuse->search($string);

    $matches = array_column($matches, 'item');
    return array_slice($matches, 0, $selection_settings['match_limit'], TRUE);
  }

  /**
   * Fetch the matching titles from the database query.
   *
   * @param string $string
   *   Text to match the label against.
   * @param string $target_type
   *   Target entity type for the autocomplete results.
   * @param array $selection_settings
   *   Selection settings array that contains field settings.
   *
   * @return array
   */
  protected function fetchEntityLabels(string $string, string $target_type, array $selection_settings, FuzzyEntityAutocomplete $handler): array {
    $matches = [];

    $cid = "fuzzy_entity_reference:{$target_type}:{$string}";

    if ($cache = $this->cache->get($cid)) {
      return $cache->data;
    }
    // Get an array of matching entities.
    $match_operator = $selection_settings['match_operator'] ?? 'CONTAINS';
    $limit = $selection_settings['query_count_limit'] ?? 20;
    $entity_labels = $handler->getReferenceableEntities($string, $match_operator, $limit);

    if (empty($entity_labels)) {
      return [];
    }

    foreach ($entity_labels as $entity_id => $label) {
      $key = "$label ($entity_id)";
      // Strip things like starting/trailing white spaces, line breaks and tags.
      $key = preg_replace('/\s\s+/', ' ', str_replace("\n", '', trim(Html::decodeEntities(strip_tags($key)))));
      // Names containing commas or quotes must be wrapped in quotes.
      $key = Tags::encode($key);
      $matches[] = ['value' => $key, 'label' => $label];
    }

    // Create a list of cache tags for setting in cache bin for invalidation.
    $cache_tags = [];
    if (!empty($selection_settings['target_bundles'])) {
      $cache_tags = array_map(function($bundle) use ($target_type) {
        return "{$target_type}_list:{$bundle}";
      }, $selection_settings['target_bundles']);
    }
    $this->cache->set($cid, $matches, Cache::PERMANENT, $cache_tags);

    return $matches;
  }

}
