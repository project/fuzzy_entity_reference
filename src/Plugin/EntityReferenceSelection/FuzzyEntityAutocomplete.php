<?php

declare(strict_types=1);

namespace Drupal\fuzzy_entity_reference\Plugin\EntityReferenceSelection;

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Plugin\EntityReferenceSelection\DefaultSelection;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * FuzzyEntityAutocomplete entity reference selection plugin.
 *
 * @EntityReferenceSelection(
 *   id = \Drupal\fuzzy_entity_reference\Plugin\EntityReferenceSelection\FuzzyEntityAutocomplete::PLUGIN_ID,
 *   label = @Translation("Fuzzy entity autocomplete"),
 *   group = "fuzzy_entity_autocomplete",
 *   weight = 0
 * )
 */
class FuzzyEntityAutocomplete extends DefaultSelection {
  public const PLUGIN_ID = 'fuzzy_entity_autocomplete';

  /**
   * Language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * Constructs a new FuzzyEntityAutocomplete object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler service.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The current user.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   The entity field manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface|null $entityTypeBundleInfo
   *   The entity type bundle info service.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entityRepository
   *   The entity repository.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager, ModuleHandlerInterface $moduleHandler, AccountInterface $currentUser, EntityFieldManagerInterface $entityFieldManager, EntityTypeBundleInfoInterface $entityTypeBundleInfo = NULL, EntityRepositoryInterface $entityRepository, LanguageManagerInterface $languageManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entityTypeManager, $moduleHandler, $currentUser, $entityFieldManager, $entityTypeBundleInfo, $entityRepository);
    $this->languageManager = $languageManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('module_handler'),
      $container->get('current_user'),
      $container->get('entity_field.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('entity.repository'),
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getReferenceableEntities($match = NULL, $match_operator = 'CONTAINS', $limit = 10): array {
    $query = $this->buildEntityQuery($match, $match_operator);
    if ($limit > 0) {
      $query->range(0, $limit);
    }
    $result = $query->execute();

    if (empty($result)) {
      return [];
    }

    $target_type = $this->getConfiguration()['target_type'];
    $entity_type = $this->entityTypeManager->getDefinition($target_type);
    $id_field = $entity_type->getKey('id');
    $label_field = $entity_type->getKey('label');
    $query = $this->entityTypeManager->getStorage($target_type)
      ->getAggregateQuery()
      ->accessCheck()
      ->groupBy($id_field)
      ->groupBy($label_field)
      ->condition($id_field, $result, 'IN');
    if ($entity_type->isTranslatable()) {
      $query->condition('langcode', $this->languageManager->getCurrentLanguage()->getId());
    }
    $options = $query->execute();
    $options = array_combine(array_column($options, $id_field), array_column($options, $label_field));
    return array_map([Html::class, 'escape'], $options);
  }

}
